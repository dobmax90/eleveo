package com.zoomint.service;

import com.zoomint.service.donotchange.CarRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration("spring-context-test.xml")
class CarManagerJuniorTest {

    @MockBean
    CarRepository carRepository;

    @Autowired
    CarManagerJunior carManagerJunior;

    private Car createCar(int carId, long horsePower, Car.Fuel fuel) {
        Car car = new Car();
        car.setCarId(carId);
        car.setHorsePower(horsePower);
        car.setCarFuel(fuel);
        return car;
    }

    private Car createCar(long horsePower, Car.Fuel fuel) {
        Car car = new Car();
        car.setHorsePower(horsePower);
        car.setCarFuel(fuel);
        return car;
    }

    @Test
    void shouldAddNothing_whenNoCarsPassedToBeStoredInGarage() throws Exception {
        assertThat(carManagerJunior.addCarsToMyGarage(Collections.emptyList())).isEmpty();
    }

    @Test
    void shouldAddAll_whenGarageIsEmpty() throws Exception {
        when(carRepository.findAllCarsInGarage()).thenReturn(Collections.emptyList());
        assertThat(
                carManagerJunior.addCarsToMyGarage(
                        List.of(createCar(15, Car.Fuel.ELECTRIC))
                )
        )
                .hasSize(1);
    }

    @Test
    void shouldAddNew_whenPassedCar_morePowerfulThanExistingOne() throws Exception {
        when(carRepository.findAllCarsInGarage()).thenReturn(List.of(createCar(14, Car.Fuel.DIESEL)));
        assertThat(carManagerJunior.addCarsToMyGarage(
                List.of(createCar(15, Car.Fuel.DIESEL)))
        )
                .hasSize(1);
    }

    @Test
    void shouldAddNothing_whenPassedCar_lessPowerfulThanExistingOne() throws Exception {
        when(carRepository.findAllCarsInGarage()).thenReturn(List.of(createCar(15, Car.Fuel.DIESEL)));
        assertThat(carManagerJunior.addCarsToMyGarage(
                List.of(createCar(14, Car.Fuel.DIESEL)))
        )
                .isEmpty();
    }

    @Test
    void shouldAddNothing_whenPassedCar_lessPowerfulThanExistingOneRegardlessEcologicalDominance() throws Exception {
        when(carRepository.findAllCarsInGarage()).thenReturn(List.of(createCar(15, Car.Fuel.DIESEL)));
        assertThat(carManagerJunior.addCarsToMyGarage(
                List.of(createCar(14, Car.Fuel.ELECTRIC)))
        )
                .isEmpty();
    }

    @Test
    void shouldAddNew_whenPassedCar_hasSamePowerButMoreEcologicalThanExistingOne() throws Exception {
        when(carRepository.findAllCarsInGarage())
                .thenReturn(List.of(createCar(15, Car.Fuel.DIESEL)));
        assertThat(
                carManagerJunior.addCarsToMyGarage(
                        List.of(createCar(15, Car.Fuel.ELECTRIC))
                )
        )
                .hasSize(1);
    }

    @Test
    void shouldAddNothing_whenPassedCar_hasSamePowerButLessEcologicalThanExistingOne() throws Exception {
        when(carRepository.findAllCarsInGarage())
                .thenReturn(List.of(createCar(15, Car.Fuel.ELECTRIC)));
        assertThat(
                carManagerJunior.addCarsToMyGarage(
                        List.of(createCar(15, Car.Fuel.DIESEL))
                )
        )
                .isEmpty();

    }

    @Test
    void shouldAddNew_whenPassedCar_thatConsideredAsMorePowerfulBetweenFewStoredInGarage() throws Exception {
        when(carRepository.findAllCarsInGarage())
                .thenReturn(
                        List.of(
                                createCar(13, Car.Fuel.DIESEL),
                                createCar(15, Car.Fuel.DIESEL)
                        )
                );
        assertThat(carManagerJunior.addCarsToMyGarage(
                List.of(createCar(14, Car.Fuel.DIESEL)))
        )
                .hasSize(1);
    }


}