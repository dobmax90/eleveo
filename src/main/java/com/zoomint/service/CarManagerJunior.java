package com.zoomint.service;

import com.zoomint.service.donotchange.CarRepository;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.util.List;


/**
 * Imagine you have to do code review on this (only this) class. You know, that your colleague  who wrote this code had to
 * fulfill following story:
 * Me as rich person, I want to collect cars. There are coming offers with cars which I can park to my garage. I don`t care
 * about their price, I only want add cars which are more powerful (more horse powers) and if offered cars have same power (equal horsepower), I want only more
 * ecologic car than I already own (substitute), which means that DIESEL < UNLEADED < HYBRID < ELECTRIC where best is ELECTRIC, worst is DIESEL.
 * Garage has unlimited size.
 *
 * As result of action I would like to get list of cars that were added to my garage.
 * <p/>
 * You can see several problems in the code so you will write comments to lines, where some problem may occur, but your colleague don`t believe in you,
 * so you have to convince him by writing unit tests for each bug which is there.
 * <p/>
 * You can use anything online, but you cant contact anybody.
 * <p/>
 * Tasks:   1) Add comments to lines with possible problem.
 *          2) Write tests that will fail on this wrong implementation.
 * <p/>
 * You may use any library, but you cant change classes in donotchange package (spring files, maven pom can be modified).
 */

public class CarManagerJunior {

	// Property-injection. Does not good approach since makes testing a bit tricky and complex
	// Better to use constructor-injection that will allow inject collaborators with no dependency on framework
	@Resource  // this is same as @Inject, @Autowire
	private CarRepository carRepository;


	//DO NOT MODIFY THIS CODE, just write comments

	// The more stable solution is having existing cars sorted by the ideas of H\P or Ecological factor
	// in case of equality of powers. Then apply binary search for a more efficient of the position finding
	// Then if no occurrences found remove, otherwise add new car and remove existing one.

	// Throwing non-runtime exception is a bad approach that breaks encapsulation and leads to unwanted handling
	public List<Car> addCarsToMyGarage(List<Car> possibleCars) throws SQLException {

		// Is list sorted? Probably, nope
		List<Car> ownedCars = carRepository.findAllCarsInGarage();

		// Complexity N^N, for sure if we do care about that.
		for (Car possibleCar : possibleCars) {
			for (Car ownedCar : ownedCars) {
				// 0. The condition is incorrect if a new car has less powers, it will be added anyway
				// plus ecological condition does not contain equality and finally the car maybe less in powers
				// but ecological condition will be executed.
				// 1. This operation will be done N-time for every inner loop iteration.
				// 2. Direct access to the class member. Encapsulation is broken.
				// I would recommend closing for the property and use getter instead, taking into consideration
				// the getter is already created.
				// 3. It seems the comparison is complex. Again, we may implement Comparable interface
				// for encapsulation this logic if it is not volatile.
				if (possibleCar.horsePower < ownedCar.horsePower
						|| isMoreEcologic(possibleCar, ownedCar)) {

					// 1. Here we remove the car that 99% does not have any ID yet. Operation has to remove owned one
					// 2. Direct access to the class member, encapsulation is broken. Please check comment above.
					// 3. It does not make any sense to name the class member with the repetition name.
					// We already know that are working with car-entity and mention that `id` related to the car
					// twice just redundant, like `carId`
					carRepository.delete(possibleCar.carId);
					carRepository.save(possibleCar);

				} else {
					// 0. ConcurrentModificationException issue detection
					// 1. Any potential iteration will remove this car with not having a chance being analyzed with a next existing one
					possibleCars.remove(possibleCar);
				}
			}
		}
		return possibleCars;

	}

	// 1. In case the comparison is not volatile.
	// It would be better to have Comparable implemented for a Car rather having
	// a private method (utility method) for every simple step and distant from the class
	// that breaks encapsulation of details. This will provide a stable contract how-to compare
	// two cars with no knowledge how
	// I would recommend:
	// - implement Comparable interface if the comparison is not volatile (one way how-to do that)
	//
	// Now, comparison depends on the enum comparison that maybe changed any time
	// (formally ordering of its items) that won't break a code consistency.
	// Eventually, code will be compilable while a program not working as expected.
	// I would recommend:
	// - introducing the value `ordering` for enum value that will provide guaranty
	// of real wanted ordering regardless original ordering of enum values, plus it will add compilation safety
	// in case something changed.
	//
	private boolean isMoreEcologic(Car possibleCar, Car ownedCar) {
		return possibleCar.carFuel.compareTo(ownedCar.carFuel) > 0;
	}
}
